<?php 

// Do not import if these conditions are met
add_filter('hsp_api_import_filter_prop_value_before', function($prop_val, $property_data, $key, $prop_config){
    $excluded = array('Not available', 'No disponible', 'Rented', 'Alquilado', 'Sold', 'Vendido');
    if($key === 'property_condition' && in_array($prop_val, $excluded))
    {
        $prop_val = null;
    }
    return $prop_val;
}, 20, 4);