
<div id="<?php echo $view->getId(); ?>" class="<?php echo $view->getHtmlClass(); ?>">

    <h1><?php _e('Properties API Import', 'hsp'); ?></h1>

    <p>&nbsp;</p>

    <p>
        <button class="button button-primary btn-import-all">
            <?php _e('Import all', 'hsp'); ?>
        </button>
    </p>

    <p>&nbsp;</p>

    <?php 
    $i = 0;

    foreach($view->get_langs() as $lang): ?>
    <div class="lang-cont">

        <h3><?php printf(__('Language: %s', 'hsp'), $lang['code']); ?></h3>
        <hr />

        <?php foreach($view->get_city_options() as $option): ?>

        <div class="import-cont city-<?php $option['id']; ?>">
            <h4><?php echo $option['name']; ?></h4>
            <div class="import-summary"></div>
            <div class="api-actions">
                <button class="button button-secondary btn-import btn-import-<?php echo $i; ?>" 
                    data-city_id="<?php echo $option['id']; ?>" 
                    data-city_name="<?php echo $option['name']; ?>" 
                    data-lang="<?php echo $lang['code']; ?>" 
                    data-i="<?php echo $i; ?>"
                    >
                        <?php _e('Import properties', 'hsp'); ?>
                </button>
            </div>
        </div>
        <?php $i++; endforeach; ?>

        <p>&nbsp;</p>

    <?php endforeach; ?>

    <script type="text/javascript">
        jQuery(function($){

            const view = $("#<?php echo $view->getId(); ?>");

            let doingAllImport = false;
            const importBtns = view.find(".btn-import");

            const importProperties = function(btn, page=1, sessionId='')
            {
                btn.prop("disabled", true);

                const cityId = parseInt(btn.data("city_id"));
                const lang = btn.data("lang");

                $.post(ajaxurl, {
                    action: "hsp_api_import",
                    page: page,
                    session_id: sessionId,
                    city_id: cityId,
                    lang: lang
                }, function(resp){

                    // console.log(resp);

                    if(typeof resp.summary_html !== 'undefined' && typeof btn !== 'undefined')
                    {
                        const importCont = btn.closest(".import-cont");
                        const summaryCont = importCont.find(".import-summary");
                        summaryCont.html(resp.summary_html);
                    }

                    if(typeof resp.page_max !== 'undefined' && page < resp.page_max)
                    {
                        page++;
                        const sessionId = (typeof resp.session_id !== 'undefined') ? resp.session_id : '';
                        importProperties(btn, page, sessionId);
                    }
                    else{
                        btn.prop("disabled", false);

                        if(doingAllImport)
                        {
                            const iNext = parseInt(btn.data("i"))+1;
                            const importBtnNext = importBtns.filter(".btn-import-" + iNext);

                            if(importBtnNext.length)
                            {
                                importProperties(importBtnNext);
                            }
                            else{
                                doingAllImport = false;
                            }
                        }
                    }

                }, 'json');
            }

            view.find(".btn-import-all").on("click", function(){

                const btn = $(this);
                const confirmText = "<?php _e('Start importing all properties?', 'hsp'); ?>";

                if(confirm(confirmText))
                {
                    doingAllImport = true;
                    importBtns.first().click();
                }
            });

            importBtns.on("click", function(){

                const btn = $(this);
                const confirmText = "<?php _e('Start importing properties for: ', 'hsp'); ?>" + btn.data("city_name") + "?";

                if(doingAllImport || confirm(confirmText))
                {
                    importProperties(btn);
                }
            });
        });
    </script>
</div>