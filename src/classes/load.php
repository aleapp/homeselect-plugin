<?php 

/*
 * Autoload classes
 * -------------------------
 */
wpseed_load_dir_classes(dirname(__FILE__) . '/Action', '\HSP\Action');
wpseed_load_dir_classes(dirname(__FILE__) . '/Filter', '\HSP\Filter');
wpseed_load_dir_classes(dirname(__FILE__) . '/Type/Reg', '\HSP\Type\Reg');
