<?php

namespace HSP;

class Api_Req
{
    const API_BASE = 'http://websiteapi.egorealestate.com';
    const API_VER  = 'v1';

    static function get($city, $resource, $headers=[])
    {
        $args = [
            'headers' => self::getDefaultHeaders($city, $headers)
        ];

        // $resource = apply_filters('hsp_api_import_resource_get', $resource, $city);
        // $args = apply_filters('hsp_api_import_args_get', $args, $city);
        
        $resp = wp_remote_get(
            self::getEndpointUrl($resource), 
            $args
        );

        $resp_status_code = (int)wp_remote_retrieve_response_code($resp);
        $resp_body = wp_remote_retrieve_body($resp);

        return [
            'req_args' => $args,
            'status_code' => $resp_status_code,
            'data' => $resp_body ? json_decode($resp_body, true) : []
        ];
    }

    static function post($city, $resource, $payload, $headers=[])
    {
        $args = [
            'headers' => self::getDefaultHeaders($city, $headers),
            // 'body' => json_encode($payload)
            'body' => $payload
        ];

        // $resource = apply_filters('hsp_api_import_resource_post', $resource, $city);
        // $args = apply_filters('hsp_api_import_args_post', $args, $city);

        $resp = wp_remote_post(
            self::getEndpointUrl($resource), 
            $args
        );

        $resp_status_code = (int)wp_remote_retrieve_response_code($resp);
        $resp_body = wp_remote_retrieve_body($resp);

        // file_put_contents(ABSPATH . '/__debug.txt', print_r([
        //     time(),
        //     self::getEndpointUrl($resource),
        //     $args,
        //     $resp_status_code,
        //     $resp_body,
        // ], true));

        $result = [
            'req_args' => $args,
            'status_code' => $resp_status_code,
            'data' => [],
            'server_error' => ''
        ];

        if($resp_status_code !== 200)
        {
            $result['server_error'] = $resp_body;
        }
        elseif($resp_body){
            $result['data'] = json_decode($resp_body, true);
        }

        return $result;
    }

    static function getEndpointUrl($resource)
    {
        return self::API_BASE . '/' . self::API_VER . $resource;
    }

    static function getDefaultHeaders($city, $headers=[])
    {
        return wp_parse_args($headers, [
            'AuthorizationToken' => self::getToken($city),
            'Accept' => 'application/json'
            // 'Content-Type' => 'application/json; charset=utf-8'
        ]);
    }

    static function getToken($city)
    {
        $city_term = is_int($city) ? get_term($city, 'city') : get_term_by('slug', $city, 'city');

        return is_a($city_term, 'WP_Term') ? get_term_meta($city_term->term_id, 'api_key', true) : false;
    }

    static function hasToken($city)
    {
        return (bool)self::getToken($city);
    }
}
