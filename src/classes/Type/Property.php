<?php 

namespace HSP\Type;

use HSP\Utils_Property;

class Property extends Post
{
    public function __construct($post=null)
    {
        $this->post_type = 'property';

        if(is_string($post))
        {
            $post = Utils_Property::getPropertyIdByReference($post);
        }

        parent::__construct($post, self::_get_props_config());
    }

    static function _get_props_config()
    {
        return [

            'post_title' => [
                'type' => 'data'
            ],
            'post_content' => [
                'type' => 'data',
                'import_key' => 'Description'
            ],

            'ref' => [
                'type' => 'meta',
                'import_key' => 'Reference'
            ],
            'import_id' => [
                'type' => 'meta',
                'import_key' => 'ID'
            ],

            'bedrooms' => [
                'type' => 'meta',
                'import_key' => 'Rooms',
                'cast' => 'integer'
            ],
            'bathrooms' => [
                'type' => 'meta',
                'import_key' => 'Bathrooms',
                'cast' => 'integer'
            ],
            'floor' => [
                'type' => 'meta',
                'import_key' => 'Floor',
                'cast' => 'integer'
            ],
            'net_area' => [
                'type' => 'meta',
                'import_key' => 'NetArea',
                'cast' => 'integer'
            ],
            'gross_area' => [
                'type' => 'meta',
                'import_key' => 'GrossArea',
                'cast' => 'integer'
            ],
            'land_area' => [
                'type' => 'meta',
                'import_key' => 'LandArea',
                'cast' => 'integer'
            ],

            'property_offer' => [
                'type' => 'taxonomy',
                // 'filter_key' => 'offer',
                'filter_label' => __('Offer', 'hs'),
                'import_key' => 'PropertyBusiness',
                'import_create_new' => false
            ],
            'property_type' => [
                'type' => 'taxonomy',
                // 'filter_key' => 'type',
                'filter_label' => __('Type', 'hs'),
                'import_key' => 'Type'
            ],

            'country' => [
                'type' => 'taxonomy',
                'filter_label' => __('Country', 'hs'),
                'import_key' => 'Country'
            ],
            // 'state' => [
            //     'type' => 'taxonomy',
            //     'filter_label' => __('State', 'hs'),
            //     'parent' => 'country',
            //     'import_key' => 'District'
            // ],
            'city' => [
                'type' => 'taxonomy',
                'filter_label' => __('City', 'hs'),
                // 'parent' => 'state',
                'import_key' => 'Municipality',
                'import_create_new' => false
            ],
            'parish' => [
                'type' => 'taxonomy',
                // 'filter_label' => __('Area', 'hs'),
                // 'filter_label_alt' => __('Select an area', 'hs'),
                'filter_label' => __('Select an area', 'hs'),
                // 'filter_empty_opt' => __('All', 'hs'),
                'parent' => 'city',
                'import_key' => 'Parish'
            ],
            // 'zone' => [
            //     'type' => 'taxonomy',
            //     'filter_label' => __('Zone', 'hs'),
            //     'parent' => 'parish',
            //     'import_key' => 'Zone'
            // ],
            'property_condition' => [
                'type' => 'taxonomy',
                'import_key' => 'Condition',
                'import_empty_skip' => true
            ],
            'property_feature' => [
                'type' => 'taxonomy',
                'import_key' => 'Features'
            ],

            'thumbnail_url' => [
                'type' => 'meta',
                'import_key' => 'Thumbnail'
            ],
            'gallery_urls' => [
                'type' => 'meta',
                'import_key' => 'Images'
            ],
            'gallery_images' => [
                'type' => 'attachment_list'
            ],

            'year' => [
                'type' => 'meta',
                'import_key' => 'Year'
            ],
            'zip_code' => [
                'type' => 'meta',
                'import_key' => 'ZipCode'
            ],

            'energy_cert' => [
                'type' => 'meta',
                'import_key' => 'EnergyCertification'
            ],
            'energy_cert_validity' => [
                'type' => 'meta',
                'import_key' => 'EnergyCertificationValidity'
            ],
            'energy_cert_number' => [
                'type' => 'meta',
                'import_key' => 'EnergyCertificationNumber'
            ],

            'sale_price' => [
                'type' => 'meta',
                'filter_input_type' => 'range_slider',
                'cast' => 'integer',
                'parent' => 'property_offer',
                'parent_meta_key' => 'property_price_meta',
                'import_key' => 'PropertyBusiness',
                // 'import_empty_skip' => true
            ],
            'rent_price' => [
                'type' => 'meta',
                'filter_input_type' => 'range_slider',
                'cast' => 'integer',
                'parent' => 'property_offer',
                'parent_meta_key' => 'property_price_meta',
                'import_key' => 'PropertyBusiness',
                // 'import_empty_skip' => true
            ],
            'tags' => [
                'type' => 'meta',
                'import_key' => 'Tags',
                'import_empty_skip' => true,
                'import_value_skip' => true
            ],

            'gps_lat' => [
                'type' => 'meta',
                'import_key' => 'GPSLat'
            ],
            'gps_lon' => [
                'type' => 'meta',
                'import_key' => 'GPSLon'
            ]
        ];
    }

    /*
    Images
    -------------------------
    */

    public function getAttachedGalleryImages()
    {
        return $this->get_meta('gallery_images', true, []);
    }

    public function getGalleryImages()
    {
        // Check post attached images
        $attached_gallery_images = $this->getAttachedGalleryImages();

        if($attached_gallery_images)
        {
            $gallery_images = [];

            foreach($attached_gallery_images as $image_id)
            {
                $gallery_image = [];

                $image_src = wp_get_attachment_image_src($image_id, 'medium_large');
                if($image_src && isset($image_src[0]))
                {
                    $gallery_image['Thumbnail'] = $image_src[0];
                }

                $image_src = wp_get_attachment_image_src($image_id, 'full');
                if($image_src && isset($image_src[0]))
                {
                    $gallery_image['Original'] = $image_src[0];
                }

                if(!empty($gallery_image))
                {
                    $gallery_images[] = $gallery_image;
                }
            }

            if(!empty($gallery_images))
            {
                return $gallery_images;
            }
        }

        // API CDN images
        return $this->get_meta('gallery_urls', true, []);
    }

    public function getThumbnailUrl()
    {
        $thumbnail_url = $this->get_meta('thumbnail_url', true);
        $attached_gallery_images = $this->getAttachedGalleryImages();

        if($attached_gallery_images || !$thumbnail_url)
        {
            $gallery_images = $this->getGalleryImages();
            if(isset($gallery_images[0]) && isset($gallery_images[0]['Thumbnail']))
            {
                $thumbnail_url = $gallery_images[0]['Thumbnail'];
            }
        }

        return $thumbnail_url;
    }

    public function getFeaturedImageSrc($size='Original')
    {
        // $thumbnail_url = $this->get_meta('thumbnail_url', true);
        $thumbnail_url = $this->getThumbnailUrl();

        if($size !== 'Thumbnail' && $thumbnail_url)
        {
            $gallery_images = $this->getGalleryImages();
            foreach($gallery_images as $gallery_image)
            {
                if(isset($gallery_image['Thumbnail']) && $thumbnail_url === $gallery_image['Thumbnail'] && isset($gallery_image[$size]))
                {
                    $thumbnail_url = $gallery_image[$size];
                    break;
                }
            }
        }

        return $thumbnail_url;
    }

    public function getGalleryImagesSrc($size='Original')
    {
        $items = [];

        $gallery_images = $this->getGalleryImages();
        foreach($gallery_images as $image)
        {
            if(isset($image[$size]))
            {
                $items[] = $image[$size];
            }
        }

        return $items;
    }

    /*
    Property details
    -------------------------
    */

    public function getId()
    {
        return $this->get_id();
    }

    public function getAddressFormatted()
    {
        return 'Property address';
    }

    public function getSalePrice($format=false)
    {
        $meta = (int)$this->get_meta('sale_price', true);

        return $format ? Utils_Property::formatPrice($meta) : $meta;
    }
    public function getRentPrice($format=false)
    {
        $meta = (int)$this->get_meta('rent_price', true);

        return $format ? Utils_Property::formatPrice($meta) : $meta;
    }
    public function getPrice($format=false)
    {
        $meta_key = Utils_Property::getPriceMetaKeyByOffer($this->getOffer());
        $meta = (int)$this->get_meta($meta_key, true);

        return $format ? Utils_Property::formatPrice($meta) : $meta;
    }

    public function isOfferSale()
    {
        return (Utils_Property::getPriceMetaKeyByOffer($this->getOffer()) === 'sale_price');
    }

    public function isOfferRent()
    {
        return (Utils_Property::getPriceMetaKeyByOffer($this->getOffer()) === 'rent_price');
    }

    public function getReference()
    {
        return $this->get_meta('ref', true);
    }

    public function getImportId()
    {
        return $this->get_meta('import_id', true);
    }

    public function getArea($formatted=false)
    {
        if($this->getGrossArea())
        {
            return $this->getGrossArea($formatted);
        }

        // if($this->getNetArea())
        // {
        //     return $this->getNetArea($formatted);
        // }

        return false;
    }

    public function getNetArea($formatted=false)
    {
        $meta = (int)$this->get_meta('net_area', true);

        return $formatted ? Utils_Property::formatArea($meta) : $meta;
    }

    public function getGrossArea($formatted=false)
    {
        $meta = (int)$this->get_meta('gross_area', true);
        
        return $formatted ? Utils_Property::formatArea($meta) : $meta;
    }

    public function getLandArea($formatted=false)
    {
        $meta = (int)$this->get_meta('land_area', true);
        
        return $formatted ? Utils_Property::formatArea($meta) : $meta;
    }

    public function getBedrooms()
    {
        return (int)$this->get_meta('bedrooms', true);
    }

    public function getBathrooms()
    {
        return (int)$this->get_meta('bathrooms', true);
    }

    public function getFloor()
    {
        return (int)$this->get_meta('floor', true);
    }

    public function getGpsLat()
    {
        return $this->get_meta('gps_lat', true);
    }

    public function getGpsLon()
    {
        return $this->get_meta('gps_lon', true);
    }

    /*
    Property terms
    -------------------------
    */

    public function getCountry($names=false)
    {
        $fields = $names ? 'names' : 'ids';
        return $this->getTerms('country', $fields, true);
    }

    public function getState($names=false)
    {
        $fields = $names ? 'names' : 'ids';
        return $this->getTerms('state', $fields, true);
    }

    public function getCity($names=false)
    {
        $fields = $names ? 'names' : 'ids';
        return $this->getTerms('city', $fields, true);
    }

    public function getParish($names=false)
    {
        $fields = $names ? 'names' : 'ids';
        return $this->getTerms('parish', $fields, true);
    }

    public function getZone($names=false)
    {
        $fields = $names ? 'names' : 'ids';
        return $this->getTerms('zone', $fields, true);
    }

    // public function getBranch($names=false)
    // {
    //     $fields = $names ? 'names' : 'ids';
    //     return $this->getTerms('branch', $fields, true);
    // }

    public function getOffer($names=false)
    {
        $fields = $names ? 'names' : 'ids';
        return $this->getTerms('property_offer', $fields, true);
    }

    public function getType($names=false)
    {
        $fields = $names ? 'names' : 'ids';
        return $this->getTerms('property_type', $fields, true);
    }

    public function getCondition($names=false)
    {
        $fields = $names ? 'names' : 'ids';
        return $this->getTerms('property_condition', $fields, true);
    }

    public function getFeatures($names=false)
    {
        $fields = $names ? 'names' : 'ids';
        return $this->getTerms('property_feature', $fields, false);
    }

    public function getLocation()
    {
        $location = [];
        if($city = $this->getCity(true))
        {
            $location[] = $city;
        }
        if($parish = $this->getParish(true))
        {
            $location[] = $parish;
        }
        return implode(', ', $location);
    }

    /*
    Property specifications
    -------------------------
    */

    public function getSpecs()
    {
        return [];
    }
}
