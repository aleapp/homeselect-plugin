<?php 

namespace HSP\Action;

use HSP\Api_Import as Import;
use HSP\Utils;
use HSP\Utils_Property;
use WPSEED\Req;

class Api_Import 
{
    var $lang;
    
    public function __construct()
    {
        $this->lang = Utils::getCurrentLanguage(true);

        add_action('wp_ajax_hsp_api_import_all', [$this, 'importPropertiesAll']);
        add_action('wp_ajax_nopriv_hsp_api_import_all', [$this, 'importPropertiesAll']);
        add_action('wp_ajax_hsp_api_import', [$this, 'importProperties']);

        add_filter('hsp_api_import_filter_prop_value_before', [$this, 'parsePropertyOffer'], 20, 4);
        add_filter('hsp_api_import_filter_prop_value_before', [$this, 'parsePropertyPrice'], 20, 4);
        add_filter('hsp_api_import_filter_prop_value_before', [$this, 'skipTagsImport'], 20, 4);
        // add_filter('hsp_api_import_skip', [$this, 'skipNoPriceProperty'], 20, 4);

        add_action('wpseed_post_inserted', [$this, 'setPropertyLanguage'], 20, 2);
        add_action('hsp_api_import_after_term_insert', [$this, 'setTermLanguage'], 20, 5);
    }

    public function importPropertiesAll()
    {
        $res = [];

        // $languages = Utils::getLanguages();

        $city_terms = get_terms([
            'taxonomy' => 'city',
            'hide_empty' => false
        ]);

        if(!is_wp_error($city_terms))
        {
            foreach($city_terms as $city_term)
            {
                // $city_term_id = function_exists('pll_get_term') ? pll_get_term($city_term->term_id, $this->lang) : $city_term->term_id;
                // if(!$city_term_id)
                // {
                //     continue;
                // }
                $city_term_id = $city_term->term_id;

                $page = 1;

                $import = new Import($city_term_id, [
                    'page' => $page,
                    'lang' => $this->lang
                ]);

                if(!$import->isImportAvailable())
                {
                    continue;
                }
        
                $import->importProperties();
                $page_max = $import->getPageMax();
                $session_id = $import->getSessionId();

                $res[] = [
                    'page' => $page,
                    'city' => $city_term_id,
                    'lang' => $this->lang,
                    'session' => $import->getSession()
                ];

                $page += 1;
                for($page; $page <= $page_max; $page++)
                {
                    $import= new Import($city_term_id, [
                        'page' => $page,
                        'lang' => $this->lang,
                        'session_id' => $session_id
                    ]);
                    $import->importProperties();

                    $res[] = [
                        'page' => $page,
                        'city' => $city_term_id,
                        'lang' => $this->lang,
                        'session' => $import->getSession()
                    ];
                }
            }
        }

        wp_send_json($res);
    }

    public function importProperties()
    {
        $res = [
            'status' =>'ok',
            'imported_refs' => [],
            'page_max' => 0,
            'session_id' => '',
            'summary_html' => ''
        ];

        $req = new Req();
        
        $city_id = $req->get('city_id', 'integer');
        $page = $req->get('page', 'integer', 1);
        $per_page = $req->get('per_page', 'integer', 10);
        $session_id = $req->get('session_id');

        $this->lang = $req->get('lang', 'string', $this->lang);

        if(function_exists('pll_get_term'))
        {
            $city_id = pll_get_term($city_id, $this->lang);
        }

        $import= new Import($city_id, [
            'page' => $page,
            'per_page' => $per_page,
            'lang' => $this->lang,
            'session_id' => $session_id
        ]);

        if(!$import->isImportAvailable())
        {
            wp_send_json($res);
            return;
        }

        $import->importProperties();

        $res['imported_refs'] = $import->getImportedRefs();
        $res['page_max'] = $import->getPageMax();
        $res['session_id'] = $import->getSessionId();
        $res['summary_html'] = '<p>' . sprintf(__('Importing page: %d/%d', 'hsp'), $page, $import->getPageMax()) . '</p>';

        wp_send_json($res);
    }

    public function parsePropertyOffer($prop_val, $property_data, $key, $prop_config)
    {
        if($key !== 'property_offer')
        {
            return $prop_val;
        }

        if(is_array($prop_val))
        {
            $_prop_vals = [];

            foreach($prop_val as $business)
            {
                $business_name = isset($business['BusinessName']) ? $business['BusinessName'] : null;

                if(!isset($business_name))
                {
                    continue;
                }

                $property_offer = Utils_Property::getTermNameByApiName($business_name, 'property_offer');

                if($property_offer)
                {
                    $_prop_vals = [$property_offer];
                }
            }

            return $_prop_vals;
        }

        return null;
    }

    public function parsePropertyPrice($prop_val, $property_data, $key, $prop_config)
    {
        if(!in_array($key, ['sale_price', 'rent_price']))
        {
            return $prop_val;
        }

        if(is_array($prop_val))
        {
            foreach($prop_val as $business)
            {
                $api_property_offer = isset($business['BusinessName']) ? $business['BusinessName'] : null;

                if(empty($api_property_offer))
                {
                    return 0;
                }

                $property_offer = Utils_Property::getTermNameByApiName($api_property_offer, 'property_offer');
                $price_key = Utils_Property::getPriceMetaKeyByOffer($property_offer);

                if(
                    ($key === $price_key)
                    && is_array($business) 
                    && isset($business['Prices']) 
                    && isset($business['Prices'][0]) 
                    && isset($business['Prices'][0]['PriceValue'])
                ){
                    $prop_val = (int)$business['Prices'][0]['PriceValue'];
                    
                    return $prop_val ? $prop_val : 0;
                }
            }
        }

        return 0;
    }

    public function skipNoPriceProperty($skip_import, $property, $property_data, $props_config)
    {
        if(!$property->getPrice())
        {
            $skip_import = true;
        }

        return $skip_import;
    }

    public function skipTagsImport($prop_val, $property_data, $key, $prop_config)
    {
        if($key === 'tags' && is_array($prop_val))
        {
            foreach($prop_val as $tag)
            {
                if(isset($tag['Name']) && $tag['Name'] === 'No web')
                {
                    return null;
                }
            }
        }

        return $prop_val;
    }

    public function setPropertyLanguage($post_id, $post)
    {
        if(function_exists('pll_set_post_language') && $post->get_type() === 'property')
        {
            pll_set_post_language($post_id, $this->lang);
        }
    }

    public function setTermLanguage($term_id, $key, $property_data, $props_config, $import)
    {
        if(function_exists('pll_set_term_language'))
        {
            pll_set_term_language($term_id, $import->lang);
        }
    }
}