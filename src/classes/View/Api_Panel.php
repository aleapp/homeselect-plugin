<?php 

namespace HSP\View;

use HSP\Api_Import;
use HSP\Utils;

class Api_Panel extends \WPSEED\View 
{
    public function __construct($args)
    {
        parent::__construct($args, [
            
            'city_options' => [],
            'langs' => Utils::getLanguages()
        ]);

        $this->setCityOptions();

        //$this->setArgsToProps(true);
    }

    protected function setCityOptions()
    {
        if(!$this->args['city_options'])
        {
            $terms = get_terms([
                'taxonomy' => 'city',
                'hide_empty' => false
            ]);

            if(!is_wp_error($terms))
            {
                foreach($terms as $term)
                {
                    $this->args['city_options'][] = [
                        'id' => $term->term_id,
                        'slug' => $term->slug,
                        'name' => $term->name
                    ];
                }
            }
        }
    }
}