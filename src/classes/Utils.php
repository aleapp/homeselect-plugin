<?php

namespace HSP;

class Utils
{
    static function getLanguages()
    {
        $langs = [];
        $langs_options = function_exists('pll_the_languages') ? pll_the_languages(['echo' => false, 'raw' => true]) : [];

        if($langs_options)
        {
            foreach($langs_options as $_lang)
            {
                $lang['code'] = isset($_lang['slug']) ? $_lang['slug'] : 'en';
                $lang['locale'] = isset($_lang['locale']) ? $_lang['locale'] : $lang['code'];
                $lang['name'] = isset($_lang['name']) ? $_lang['name'] : '';
                $lang['url'] = isset($_lang['url']) ? $_lang['url'] : '';
                $lang['current'] = isset($_lang['current_lang']) ? (bool)$_lang['current_lang'] : false;

                $langs[$lang['code']] = $lang;
            }
        }

        return $langs;
    }

    static function getLanguageParam($lang_code, $param)
    {
        $languages = self::getLanguages();
        return (isset($languages[$lang_code]) && isset($languages[$lang_code][$param])) ? $languages[$lang_code][$param] : false;
    }

    static function getCurrentLanguage($as_code=true)
    {
        // Customizer fix
        if(!isset($_REQUEST['lang']) && is_admin() && isset($_GET['customize_changeset_uuid']) && Session::getCookie('lang'))
        {
            return $as_code ? Session::getCookie('lang') : self::getLanguageParam(Session::getCookie('lang'), 'locale');
        }

        $locale = function_exists('pll_current_language') ? pll_current_language('locale') : get_locale();
        $locale = str_replace('_', '-', $locale);

        return $as_code ? substr($locale, 0, 2) : $locale;
    }

    static function getDefaultLanguage($as_code=true)
    {
        $locale = function_exists('pll_default_language') ? pll_default_language('locale') : get_locale();
        $locale = str_replace('_', '-', $locale);

        return $as_code ? substr($locale, 0, 2) : $locale;
    }

    static function parseArrInts($arr)
    {
        $_arr = [];
        if(!empty($arr))
        {
            foreach($arr as $arr_item)
            {
                $_arr[] = (int)$arr_item;
            }
        }
        return $_arr;
    }
    
    static function removeArrEmptyVals($arr, $empty_vals=[''])
    {
        if(!is_array($arr))
        {
            return $arr;
        }
        
        $_arr = [];
        if(!empty($arr))
        {
            foreach($arr as $i => $arr_item)
            {
                if(!in_array($arr_item, $empty_vals, true))
                {
                    $_arr[$i] = $arr_item;
                }
            }
        }
        return $_arr;
    }

    static function getPostTypeSelectOptions($post_type='page')
    {
        $options = [
            0 => __('Select -- ', 'hs')
        ];

        $posts_q = new \WP_Query([
            'post_type' => $post_type,
            'posts_per_page' => 100,
            'post_status' => 'publish',
            'orderby' => 'title',
            'order' => 'ASC'
        ]);

        if($posts_q->posts)
        {
            foreach($posts_q->posts as $post)
            {
                $options[$post->ID] = $post->post_title;
            }
        }

        return $options;
    }

    static function getTermData($field, $term, $taxonomy)
    {
        $wp_term = is_int($term) ? get_term($term, $taxonomy) : get_term_by('slug', $term, $taxonomy);

        if(is_a($wp_term, 'WP_Term') && isset($wp_term->$field))
        {
            return $wp_term->$field;
        }

        return null;
    }
}
