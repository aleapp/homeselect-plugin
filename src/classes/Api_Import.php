<?php

namespace HSP;

use HSP\Type\Property;
use HSP\Utils_Property;
use HSP\Utils;

class Api_Import
{
    var $city;
    var $city_slug;
    var $lang;

    var $page;
    var $per_page;
    var $page_max;

    var $delete_properties;

    var $time;
    var $resource_name;

    var $session_id;
    var $session_file;
    var $session;
    var $log_file;
    var $log;

    const RESOURCE_NAME_DEFAULT = '/Properties';
    const PAGE_MAX = 0; #set to 0 to disable
    const IMPORT_DIR_NAME = 'hs-import';
    const LOG_IMPORT = true;
    const EMAIL_LOG = true;

    public function __construct($city_id, $args=[])
    {
        $args = wp_parse_args($args, [
            'page' => 1,
            'per_page' => 50,
            'session_id' => '',
            'lang' => Utils::getCurrentLanguage(true),
            'delete_properties' => true
        ]);

        $this->city = (int)$city_id;
        $this->city_slug = Utils::getTermData('slug', $this->city, 'city');
        $this->page = $args['page'];
        $this->per_page = $args['per_page'];
        $this->lang = $args['lang'];
        $this->delete_properties = $args['delete_properties'];

        $this->page_max = 1;

        $this->time = gmdate('Y-m-d--H-i-s');

        $this->setResourceName();

        $this->setSession($args['session_id']);
    }

    public function isImportAvailable()
    {
        return Api_Req::hasToken($this->city);
    }

    public function setPage($page=1)
    {
        $this->page = $page;
    }

    public function setPerPage($per_page=1)
    {
        $this->per_page = $per_page;
    }

    public function getPageMax()
    {
        return $this->page_max;
    }

    public function getImportedRefs()
    {
        return $this->session['imported_refs'];
    }

    public function getTime()
    {
        return $this->time;
    }

    public function getSessionId()
    {
        return $this->session_id;
    }

    public function getSession()
    {
        return $this->session;
    }

    protected function setResourceName()
    {
        $resource_name = $this->city ? get_term_meta($this->city, 'api_resource_name', true) : false;
        $this->resource_name = $resource_name ? $resource_name : self::RESOURCE_NAME_DEFAULT;
    }

    protected function setSession($session_id='')
    {
        $this->session_id = $session_id ? $session_id : time();

        $uploads_dir = wp_get_upload_dir();
        $basedir = isset($uploads_dir['basedir']) ? $uploads_dir['basedir'] : null;

        if(!file_exists($basedir))
        {
            mkdir($basedir, 0755);
        }

        $import_dir = isset($basedir) ? $basedir . '/' . self::IMPORT_DIR_NAME : null;

        if(!file_exists($import_dir))
        {
            mkdir($import_dir, 0755);
        }

        $this->session_file = isset($import_dir) ? $import_dir . '/session' : null;
        $this->session = [
            'imported_refs' => [],
            'skipped_refs' => [],
            'deleted_refs' => [],
            'server_error' => ''
        ];
        if($this->page === 1)
        {
            $this->saveSession();
        }
        elseif(isset($this->session_file) && file_exists($this->session_file)){
            $this->session = json_decode(file_get_contents($this->session_file), true);
        }
        
        $log_dir = isset($import_dir) ? $import_dir . '/log' : null;

        if(!file_exists($log_dir))
        {
            mkdir($log_dir, 0755);
        }

        if(isset($log_dir))
        {
            $this->log_file = $log_dir . '/log-' . $this->city_slug . '-' . gmdate('Y-m-d--H-i-s', $this->session_id);
        }
    }

    public function importProperties()
    {
        if(!$this->isImportAvailable())
        {
            return;
        }

        $api_resp = Api_Req::post($this->city, $this->resource_name, [
            'PAG' => $this->page,
            'NRE' => $this->per_page
        ], [
            'Language' => Utils::getLanguageParam($this->lang, 'locale')
        ]);

        // Skip import and prevent delete properties
        if($api_resp['status_code'] !== 200)
        {
            $this->session['server_error'] = $api_resp['server_error'];            
            $this->logImport();

            $this->delete_properties = false;

            return;
        }

        $total = isset($api_resp['data']['TotalRecords']) ? (int)$api_resp['data']['TotalRecords'] : 0;

        if($total > $this->per_page)
        {
            $this->page_max = ceil($total / $this->per_page);
        }

        if(self::PAGE_MAX && $this->page_max > self::PAGE_MAX)
        {
            $this->page_max = self::PAGE_MAX;
        }

        $properties_data = isset($api_resp['data']['Properties']) ? $api_resp['data']['Properties'] : [];

        $insert_result = $this->insertProperties($properties_data);

        $this->session['imported_refs'] = array_merge($this->session['imported_refs'], $insert_result['inserted']);
        $this->session['skipped_refs'] = array_merge($this->session['skipped_refs'], $insert_result['skipped']);

        $this->logImport();

        if($this->page >= $this->page_max)
        {
            $this->deleteProperties();
            $this->deleteSession();
        }
        else{
            $this->saveSession();
        }
    }

    protected function insertProperties($properties_data)
    {
        $result = [
            'inserted' => [],
            'skipped' => []
        ];

        foreach($properties_data as $property_data)
        {
            if(!empty($property_data['Reference']))
            {
                $skip_import = false;

                $property = new Property($property_data['Reference']);
                $props_config = $property->get_props_config();

                foreach($props_config as $key => $prop_config)
                {
                    if(isset($prop_config['import_key']))
                    {
                        $import_key = $prop_config['import_key'];

                        $import_create_new = isset($prop_config['import_create_new']) ? $prop_config['import_create_new'] : true;
                        $import_empty_skip = isset($prop_config['import_empty_skip']) ? $prop_config['import_empty_skip'] : false;
                        $import_value_skip = isset($prop_config['import_value_skip']) ? $prop_config['import_value_skip'] : false;

                        $prop_val = isset($property_data[$import_key]) ? $property_data[$import_key] : null;
                        
                        if(isset($prop_config['default']) && empty($prop_val))
                        {
                            $prop_val = $prop_config['default'];
                        }

                        $prop_val = apply_filters('hsp_api_import_filter_prop_value_before', $prop_val, $property_data, $key, $prop_config);

                        if(!isset($prop_val))
                        {
                            if($import_empty_skip)
                            {
                                $skip_import = true;
                            }

                            continue;
                        }

                        if($import_value_skip)
                        {
                            continue;
                        }

                        if(isset($prop_config['cast']))
                        {
                            $prop_val = self::castValue($prop_val, $prop_config['cast']);
                        }

                        // if($key == 'gallery_urls')
                        // {
                        //     $gallery_urls = [];
                        //     foreach($prop_val as $image)
                        //     {
                        //         $gallery_url = [];
                        //         if(isset($image['Thumbnail']))
                        //         {
                        //             $gallery_url['thumbnail'] = $image['Thumbnail'];
                        //         }
                        //         if(isset($image['Original']))
                        //         {
                        //             $gallery_url['original'] = $image['Original'];
                        //         }
                        //         if(isset($image['Thumbnail_1280x960']))
                        //         {
                        //             $gallery_url['thumbnail_1280x960'] = $image['Thumbnail_1280x960'];
                        //         }
                        //     }
                        // }

                        switch($prop_config['type'])
                        {
                            case 'data':

                                $property->set_data($key, $prop_val);
                                break;

                            case 'meta':

                                $property->set_meta($key, $prop_val);
                                break;

                            case 'taxonomy':

                                //Set property city
                                if($key === 'city' && $this->city)
                                {
                                    $property->set_terms('city', [$this->city]);
                                    continue;
                                }

                                $prop_val = (array)$prop_val;

                                foreach($prop_val as $_prop_val)
                                {
                                    // if(!is_string($_prop_val))
                                    // {
                                    //     $_prop_val = '';
                                    // }
                                    
                                    if(empty($_prop_val))
                                    {
                                        continue;
                                    }

                                    if(
                                        isset($prop_config['parent']) 
                                        && !$property->getTerms($prop_config['parent'], 'ids', true)
                                    ){
                                        continue;
                                    }

                                    $term_id = 0;

                                    // $term_e = get_term_by('name', $_prop_val, $key);
                                    $term_e_slug = isset($prop_config['parent']) ? Utils_Property::getTermImportSlug($_prop_val, $property->getTerms($prop_config['parent'], 'ids', true), $this->lang) : Utils_Property::getTermImportSlug($_prop_val, null, $this->lang);
                                    $term_e = get_term_by('slug', $term_e_slug, $key);

                                    // Check term language
                                    if(
                                        is_a($term_e, 'WP_Term') 
                                        && function_exists('pll_get_term_language') 
                                        && pll_get_term_language($term_e->term_id, 'slug') !== Utils::getCurrentLanguage(true)
                                    ){
                                        $term_e = null;
                                    }

                                    if(is_a($term_e, 'WP_Term'))
                                    {
                                        $term_id = $term_e->term_id;
                                    }
                                    elseif($import_create_new){

                                        $term_c_slug = isset($prop_config['parent']) ? Utils_Property::getTermImportSlug($_prop_val, $property->getTerms($prop_config['parent'], 'ids', true), $this->lang) : Utils_Property::getTermImportSlug($_prop_val, null, $this->lang);

                                        $term_c = wp_insert_term($_prop_val, $key, [
                                            'slug' => $term_c_slug
                                        ]);

                                        if(!is_wp_error($term_c) && isset($term_c['term_id']))
                                        {
                                            $term_id = $term_c['term_id'];

                                            do_action('hsp_api_import_after_term_insert', $term_id, $key, $property_data, $props_config, $this);
                                        }
                                    }

                                    if($term_id)
                                    {
                                        $property->set_terms($key, [$term_id]);

                                        //Assign parent from another taxonomy
                                        $parent_key = isset($prop_config['parent']) ? $prop_config['parent'] : null;
                                        $parent_i_key = (isset($parent_key) && isset($props_config[$parent_key]) && isset($props_config[$parent_key]['import_key'])) ? $props_config[$parent_key]['import_key'] : null;

                                        if(isset($property_data[$parent_i_key]))
                                        {
                                            $parent_term_id = 0;

                                            if($parent_key === 'city')
                                            {
                                                $parent_term_id = $this->city;
                                            }
                                            else{
                                                // $parent_term = get_term_by('name', $property_data[$parent_i_key], $parent_key);
                                                $parent_term_slug = Utils_Property::getTermImportSlug($property_data[$parent_i_key], $property->getTerms($prop_config['parent'], 'ids', true), $this->lang);
                                                $parent_term = get_term_by('slug', $parent_term_slug, $parent_key);
                                                $parent_term_id = is_a($parent_term, 'WP_Term') ? $parent_term->term_id : 0;
                                            }

                                            if($parent_term_id)
                                            {
                                                update_term_meta($term_id, 'parent_term_id', $parent_term_id);
                                            }
                                        }
                                    }
                                }
                                break;
                        }
                    }
                }

                //Set property title
                // $property->set_data('post_title', sprintf(__('Property %s', 'hsp'), $property_data['Reference']));
                $property->set_data('post_title', $property_data['Reference']);

                //Set property city
                // if($this->city)
                // {
                //     $property->set_terms('city', [$this->city]);
                // }

                //Create/save property
                $skip_import = apply_filters('hsp_api_import_skip', $skip_import, $property, $property_data, $props_config);

                if($skip_import)
                {
                    $result['skipped'][] = $property_data['Reference'];
                }
                else{
                    $property = apply_filters('hsp_api_import_before_post_persist', $property, $property_data, $props_config);

                    $property->persist();

                    if($property->get_id())
                    {
                        $result['inserted'][] = $property_data['Reference'];
                    }

                    do_action('hsp_api_import_after_property_persist', $property, $property_data, $props_config);
                }
            }
        }

        return $result;
    }

    protected function deleteProperties()
    {
        if(!($this->delete_properties && file_exists($this->session_file)))
        {
            return;
        }

        $q = new \WP_Query([
            'post_type' => 'property',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'tax_query' => [
                [
                    'taxonomy' => 'city',
                    'field' => 'term_id',
                    'terms' => [$this->city],
                    'operator' => 'IN'
                ]
            ],
            'fields' => 'ids'
        ]);

        $imported_refs = $this->getImportedRefs();

        foreach($q->posts as $post_id)
        {
            $ref = get_post_meta($post_id, 'ref', true);
            
            if($ref)
            {
                // Remove possible duplicates
                Utils_Property::deletePropertiesByReference($ref, [$post_id]);

                // Remove properties not included in $imported_refs
                if(!in_array($ref, $imported_refs))
                {
                    wp_delete_post($post_id, true);
                    $this->session['deleted_refs'][] = $ref;
                }
            }
        }
    }

    protected function logImport()
    {
        if(!self::LOG_IMPORT)
        {
            return;
        }

        $n = "\n\r";

        $_log_info = [
            sprintf(__('Time: %s', 'hsp'), $this->time),
            sprintf(__('Page: %d', 'hsp'), $this->page),
            // sprintf(__('City: %s', 'hsp'), $this->city),
            sprintf(__('City: %s', 'hsp'), $this->city_slug),
            sprintf(__('Lang: %s', 'hsp'), $this->lang),
            sprintf(__('Imported refs (%d): %s', 'hsp'), count($this->session['imported_refs']), implode(', ', $this->session['imported_refs'])),
            sprintf(__('Skipped refs (%d): %s', 'hsp'), count($this->session['skipped_refs']), implode(', ', $this->session['skipped_refs'])),
            sprintf(__('Deleted refs (%d): %s', 'hsp'), count($this->session['deleted_refs']), implode(', ', $this->session['deleted_refs'])) . $n,
            sprintf(__('Server error: %s', 'hsp'), $this->session['server_error']) . $n,
            str_repeat('-', 50) . $n
        ];

        $log_info = implode($n, $_log_info);

        file_put_contents($this->log_file, $log_info, FILE_APPEND);
    }

    protected function saveSession()
    {
        file_put_contents($this->session_file, json_encode($this->session));
    }

    protected function deleteSession()
    {
        if(file_exists($this->session_file))
        {
            unlink($this->session_file);
        }
    }

    static function castValue($val, $type)
    {
        switch($type)
        {
            case 'integer':
                $val = intval($val);
                break;
            case 'float':
                $val = floatval($val);
                break;
        }
        return $val;
    }
}
