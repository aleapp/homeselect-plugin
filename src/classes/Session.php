<?php

namespace HSP;

class Session
{
    const COOKIE_PREFIX = 'hsp_';
    const COOKIE_PATH = '/';

    static function setCookie($key, $val)
    {
        return setcookie(self::COOKIE_PREFIX . $key, $val, self::getCookieTime(), self::COOKIE_PATH);
    }

    static function delCookie($key)
    {
        return setcookie(self::COOKIE_PREFIX . $key, $val, self::getCookieTime(true), self::COOKIE_PATH);
    }

    static function getCookie($key, $default=null)
    {
        return isset($_COOKIE[self::COOKIE_PREFIX . $key]) ? $_COOKIE[self::COOKIE_PREFIX . $key] : $default;
    }

    static function getCookieTime($del_time=false)
    {
        $time = time()+60*60*24*30;

        return $del_time ? (0-$time) : $time;
    }
}
