<?php 

namespace HSP\Action;

use HSP\Session;

class Lang 
{
    public function __construct()
    {
        add_action('admin_init', __CLASS__ . '::setWpLang');
    }

    static function setWpLang()
    {
        if(isset($_GET['lang']))
        {
            Session::setCookie('lang', $_GET['lang']);
        }
    }
}