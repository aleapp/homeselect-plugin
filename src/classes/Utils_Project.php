<?php

namespace HSP;

use HSP\Type\Project;
use HSP\Utils;

class Utils_Project
{
    static function getProjects($q_args, $debug=false)
    {
        $_q_args = [
            'post_type' => 'project',
            'post_status' => 'publish',
            'paged' => 1,
            'posts_per_page' => 10,
            'meta_query' => [],
            'tax_query' => [],
            'posts__in' => [],
            'posts__not_in' => [],
            'orderby' => 'menu_order',
            'order' => 'ASC'
        ];

        $q_args = wp_parse_args($q_args, $_q_args);

        //Set meta/taxonomy
        foreach(Project::_get_props_config() as $key => $prop_config)
        {
            $type = isset($prop_config['type']) ? $prop_config['type'] : 'meta';
            $filter_input_type = isset($prop_config['filter_input_type']) ? $prop_config['filter_input_type'] : 'text';
            $q = isset($prop_config['q']) ? $prop_config['q'] : [];
            $value = isset($q_args[$key]) ? $q_args[$key] : (isset($q['default']) ? $q['default'] : '');

            if(empty($value))
            {
                continue;
            }

            switch($type)
            {
                case 'meta':

                    $q_args['meta_query'][] = [
                        'key' => $key,
                        'value' => $value,
                        'type' => isset($q['type']) ? $q['type'] : 'CHAR',
                        'compare' => isset($q['compare']) ? $q['compare'] : '='
                    ];
                    
                    break;
                case 'taxonomy':

                    $field = isset($q['field']) ? $q['field'] : 'term_id';
                    if(!is_array($value))
                    {
                        $value = [$value];
                    }
                    $q_args['tax_query'][] = [
                        'taxonomy' => $key,
                        'field' => $field,
                        'terms' => ($field === 'term_id') ? Utils::parseArrInts($value) : $value,
                        'operator' => isset($q['operator']) ? $q['operator'] : 'IN'
                    ];
                    break;
            }
        }

        if(count($q_args['meta_query']) > 1)
        {
            $q_args['meta_query']['relation'] = 'AND';
        }
        if(count($q_args['tax_query']) > 1)
        {
            $q_args['tax_query']['relation'] = 'AND';
        }

        // Unset extra keys
        $_q_keys = array_keys($_q_args);
        foreach(array_keys($q_args) as $key)
        {
            if(!in_array($key, $_q_keys))
            {
                unset($q_args[$key]);
            }
        }

        $wp_query = new \WP_Query($q_args);

        // if($debug)
        // {
        //     print_r([$q_args, $wp_query->posts]);
        //     exit;
        // }
        // file_put_contents(ABSPATH . '/__debug.txt', print_r([time(), $q_args, count($wp_query->posts), $wp_query->found_posts, $_POST], true));

        $items = [];

        if($wp_query->posts)
        {
            foreach($wp_query->posts as $post)
            {
                $items[] = new Project($post);
            }
        }

        return [
            'items' => $items,
            'items_total' => $wp_query->found_posts
        ];
    }
}
