<?php

namespace HSP;

use HSP\Type\Agent;

class Utils_Agent
{
    static function getAgents($city_id=null)
    {
        $users = [];

        $_q_args = [
            'role' => 'subscriber',
            'meta_query' => [
                [
                    'key' => 'is_agent',
                    'value' => 1,
                    'type' => 'NUMERIC',
                    'compare' => '='
                ]
            ]
        ];
        if(isset($city_id))
        {
            if(function_exists('pll_get_term'))
            {
                $city_id = pll_get_term($city_id, Utils::getCurrentLanguage());
            }

            $_q_args['meta_query'][] = [
                'key' => 'agent_city',
                'value' => $city_id,
                'type' => 'NUMERIC',
                'compare' => '='
            ];
        }

        if(count($_q_args['meta_query']) > 1)
        {
            $_q_args['meta_query']['relation'] = 'AND';
        }

        $_users = get_users($_q_args);

        if($_users)
        {
            foreach($_users as $_user)
            {
                $users[] = new Agent($_user);
            }
        }

        return $users;
    }

    static function getRandomAgent($city_id=null, $default=null)
    {
        $agents = self::getAgents($city_id);

        if($agents)
        {
            shuffle($agents);
            return $agents[0];
        }
        
        return $default;
    }

    static function getCityAgent($city_id, $default=null)
    {
        $agent_id = (int)get_term_meta($city_id, 'agent', true);

        $agent = $agent_id ? new Agent($agent_id) : null;

        return isset($agent) ? $agent : $default;
    }
}
