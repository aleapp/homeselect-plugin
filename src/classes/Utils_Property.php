<?php

namespace HSP;

use HSP\Type\Property;

class Utils_Property
{
    static function getPropertyIdByReference($ref)
    {
        $q = new \WP_Query([
            'post_type' => 'property',
            'post_status' => 'publish',
            'meta_key' => 'ref',
            'meta_value' => $ref,
            'fields' => 'ids',
            'posts_per_page' => 1,
            'orderby' => 'date',
            'order' => 'ASC'
        ]);

        $post_id = isset($q->posts[0]) ? $q->posts[0] : 0;

        $current_lang = Utils::getCurrentLanguage(true);
        $post_lang = function_exists('pll_get_post_language') ? pll_get_post_language($post_id, 'slug') : false;

        $_post_id = (!$post_lang || ($post_lang === $current_lang)) ? $post_id : 0;

        return $_post_id;
    }

    static function deletePropertiesByReference($ref, $exclude=[])
    {
        $deleted = [];
        
        $q_args = [
            'post_type' => 'property',
            'post_status' => 'publish',
            'meta_key' => 'ref',
            'meta_value' => $ref,
            'fields' => 'ids',
            'posts_per_page' => -1,
            'post__not_in' => $exclude,
            'orderby' => 'date',
            'order' => 'ASC'
        ];
        $q = new \WP_Query($q_args);
        
        if($q->posts)
        {
            foreach($q->posts as $post_id)
            {
                wp_delete_post($post_id, true);
                $deleted[] = $post_id;
            }
        }
        
        return $deleted;
    }

    static function getProperties($q_args, $debug=false)
    {
        $price_name = !empty($q_args['sale_price']) ? 'sale_price' : 'rent_price';

        $_q_args = [
            'post_type' => 'property',
            'post_status' => 'publish',
            'paged' => 1,
            'posts_per_page' => 10,
            'meta_query' => [],
            'tax_query' => [],
            'posts__in' => [],
            'posts__not_in' => [],
            // 'orderby' => 'date',
            'orderby' => 'price_high_low',
            'order' => 'ASC'
        ];

        $q_args = wp_parse_args($q_args, $_q_args);
        // file_put_contents(ABSPATH . '/__debug.txt', print_r([time(), $q_args, $_q_args, $_POST], true));

        //Set orderby
        switch($q_args['orderby'])
        {
            case 'price_low_high':
                $q_args['orderby'] = 'meta_value';
                $q_args['order'] = 'ASC';
                $q_args['meta_key'] = $price_name;
                $q_args['meta_type'] = 'NUMERIC';
                break;
            case 'price_high_low':
                $q_args['orderby'] = 'meta_value';
                $q_args['order'] = 'DESC';
                $q_args['meta_key'] = $price_name;
                $q_args['meta_type'] = 'NUMERIC';
            break;
            case 'bedrooms_less_more':
                $q_args['orderby'] = 'meta_value';
                $q_args['order'] = 'DESC';
                $q_args['meta_key'] = $price_name;
                $q_args['meta_type'] = 'NUMERIC';
                break;
            case 'bedrooms_more_less':
                $q_args['orderby'] = 'meta_value';
                $q_args['order'] = 'DESC';
                $q_args['meta_key'] = $price_name;
                $q_args['meta_type'] = 'NUMERIC';
                break;
        }

        //Set meta/taxonomy
        foreach(Property::_get_props_config() as $key => $prop_config)
        {
            $type = isset($prop_config['type']) ? $prop_config['type'] : 'meta';
            $filter_input_type = isset($prop_config['filter_input_type']) ? $prop_config['filter_input_type'] : 'text';
            $q = isset($prop_config['q']) ? $prop_config['q'] : [];
            $value = isset($q_args[$key]) ? $q_args[$key] : (isset($q['default']) ? $q['default'] : '');

            if(empty($value))
            {
                continue;
            }

            switch($type)
            {
                case 'meta':
                    if($filter_input_type === 'range_slider' && strpos($value, '-') !== false)
                    {
                        $value = explode('-', $value);
                        $q_args['meta_query'][] = [
                            [
                                'key' => $key,
                                'value' => $value[0],
                                'type' => 'NUMERIC',
                                'compare' => '>='
                            ],
                            [
                                'key' => $key,
                                'value' => $value[1],
                                'type' => 'NUMERIC',
                                'compare' => '<='
                            ],
                            'compare' => 'AND'
                        ];
                    }
                    else{
                        $q_args['meta_query'][] = [
                            'key' => $key,
                            'value' => $value,
                            'type' => isset($q['type']) ? $q['type'] : 'CHAR',
                            'compare' => isset($q['compare']) ? $q['compare'] : '='
                        ];
                    }
                    break;
                case 'taxonomy':
                    $field = isset($q['field']) ? $q['field'] : 'term_id';
                    if(!is_array($value))
                    {
                        $value = [$value];
                    }
                    $q_args['tax_query'][] = [
                        'taxonomy' => $key,
                        'field' => $field,
                        'terms' => ($field === 'term_id') ? Utils::parseArrInts($value) : $value,
                        'operator' => isset($q['operator']) ? $q['operator'] : 'IN'
                    ];
                    break;
            }
        }

        if(count($q_args['meta_query']) > 1)
        {
            $q_args['meta_query']['relation'] = 'AND';
        }
        if(count($q_args['tax_query']) > 1)
        {
            $q_args['tax_query']['relation'] = 'AND';
        }

        // Unset extra keys
        $_q_keys = array_keys($_q_args);
        foreach(array_keys($q_args) as $key)
        {
            if(!in_array($key, $_q_keys))
            {
                unset($q_args[$key]);
            }
        }

        $wp_query = new \WP_Query($q_args);

        // if($debug)
        // {
        //     print_r([$q_args, $wp_query->posts]);
        //     exit;
        // }
        // file_put_contents(ABSPATH . '/__debug.txt', print_r([time(), $q_args, count($wp_query->posts), $wp_query->found_posts, $_POST], true));

        $items = [];

        if($wp_query->posts)
        {
            foreach($wp_query->posts as $post)
            {
                $items[] = new Property($post);
            }
        }

        return [
            'items' => $items,
            'items_total' => $wp_query->found_posts
        ];
    }

    static function formatNumber($num, $decimal=0)
    {
        return Utils_Format::formatNumber($num, $decimal);
    }

    static function getCurrencySymbol()
    {
        return Utils_Format::getCurrencySymbol();
    }

    static function getLengthUnit()
    {
        return Utils_Format::getLengthUnit();
    }

    static function getAreaUnit()
    {
        return Utils_Format::getAreaUnit();
    }

    static function formatPrice($price)
    {
        return Utils_Format::formatPrice($price);
    }

    static function formatArea($area)
    {
        return Utils_Format::formatArea($area);
    }

    static function getPriceMetaKeyByOffer($offer)
    {
        if(is_string($offer))
        {
            $offer_term = get_term_by('name', $offer, 'property_offer');
            $offer = is_a($offer_term, 'WP_Term') ? $offer_term->term_id : 0;
        }

        $price_key = $offer ? get_term_meta($offer, 'property_price_meta', true) : false;

        return $price_key ? $price_key : 'sale_price';
    }

    static function getTermNameByApiName($api_name, $taxonomy)
    {
        $terms = get_terms([
            'taxonomy' => $taxonomy,
            'hide_empty' => false,
            'meta_key' => 'api_import_name',
            'meta_value' => $api_name,
            'fields' => 'names'
        ]);

        if(is_wp_error($terms))
        {
            return false;
        }

        return isset($terms[0]) ? $terms[0] : false;
    }

    static function getTermImportSlug($term_name, $parent_id=null, $lang=null)
    {
        $slug = sanitize_title($term_name);
        if(!empty($parent_id))
        {
            $slug .= '-' . $parent_id;
        }
        if(!empty($lang))
        {
            $slug .= '-' . $lang;
        }
        return $slug;
    }
}
