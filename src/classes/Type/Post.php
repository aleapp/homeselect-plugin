<?php 

namespace HSP\Type;

use WPSEED\Post as Type_Post;

class Post extends Type_Post
{
    public function __construct($post=null, $props_config=[])
    {
        parent::__construct($post, $props_config);
    }

    static function _get_props_config()
    {
        return [];
    }

    static function getPropConfigData($key, $data_key=null, $default=null)
    {
        $props_config = static::_get_props_config();

        $prop_config = isset($props_config[$key]) ? $props_config[$key] : null;

        if(isset($data_key))
        {
            return isset($prop_config[$data_key]) ? $prop_config[$data_key] : (isset($default) ? $default : null);
        }

        return isset($prop_config) ? $prop_config : (isset($default) ? $default : $prop_config);
    }

    public function getId()
    {
        return $this->get_id();
    }

    public function getLink()
    {
        return $this->get_permalink();
    }

    public function getTitle()
    {
        return $this->get_data('post_title', '');
    }

    public function getDescription($autop=false)
    {
        $data = $this->get_data('post_content', '');

        return ($autop && $data) ? wpautop($data) : $data;
    }

    /*
    Images
    -------------------------
    */

    public function getFeaturedImageId()
    {
        return get_post_thumbnail_id($this->get_id());
    }

    public function getFeaturedImage($size='thumbnail', $rel_class='rect-150-100')
    {
        $image_id = $this->getFeaturedImageId();
        return $image_id ? $this->getAttachmentImageHtml($image_id, $size, $rel_class) : '';
    }

    /*
    Helpers
    -------------------------
    */

    public function getTerms($taxonomy, $fields='ids', $single=true)
    {
        $term_ids = $this->get_terms($taxonomy);

        if($fields === 'ids')
        {
            return $single ? (isset($term_ids[0]) ? $term_ids[0] : false) : $term_ids;
        }

        $args = [
            'taxonomy' => $taxonomy,
            'include' => $term_ids,
            'fields' => $fields
        ];

        $terms = get_terms($args);

        if(is_wp_error($terms))
        {
            return $single ? false : [];
        }

        if($single)
        {
            return isset($terms[0]) ? $terms[0] : false;
        }

        return $terms;
    }

    // protected function getTerms($taxonomy, $names=false, $single=true, $as_array=false)
    // {
    //     if($names)
    //     {
    //         return $as_array ? $this->getTermsNames($taxonomy) : $this->getTermsNames($taxonomy, ', ');
    //     }

    //     $term_ids = $this->get_terms($taxonomy, []);

    //     if($single)
    //     {
    //         return isset($term_ids[0]) ? $term_ids[0] : false;
    //     }
        
    //     return $term_ids;
    // }

    // protected function getTermsNames($taxonomy, $concat=false)
    // {
    //     $terms = get_terms([
    //         'taxonomy' => $taxonomy,
    //         'hide_empty' => false,
    //         'include' => $this->get_terms($taxonomy, []),
    //         'fields' => 'names'
    //     ]);
    //     $_terms = is_wp_error($terms) ? [] : $terms;

    //     return $concat ? implode($concat, $_terms) : $_terms;
    // }
}
