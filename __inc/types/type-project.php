<?php

/*
* project post_type
* -------------------------
*/

add_action('init', 'hsp_add_post_type_project');

function hsp_add_post_type_project()
{
    $labels = [
        'name'               => _x('Projects', 'post type general name', 'hsp'),
        'singular_name'      => _x('Project', 'post type singular name', 'hsp'),
        'menu_name'          => _x('Projects', 'admin menu', 'hsp'),
        'name_admin_bar'     => _x('Projects', 'add new on admin bar', 'hsp'),
        'add_new'            => _x('Add project', 'project type', 'hsp'),
        'add_new_item'       => __('Add new project', 'hsp'),
        'new_item'           => __('New project', 'hsp'),
        'edit_item'          => __('Edit project', 'hsp'),
        'view_item'          => __('View project', 'hsp'),
        'all_items'          => __('All projects', 'hsp'),
        'search_items'       => __('Search projects', 'hsp'),
        'parent_item_colon'  => __('Projecty parent:', 'hsp'),
        'not_found'          => __('No projects found.', 'hsp'),
        'not_found_in_trash' => __('No projects found in trash.', 'hsp')
    ];
    $args = [
        'labels'              => $labels,
        'description'         => __('Projecty post type.', 'hsp'),
        'public'              => true,
        'publicly_queryable'  => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'query_var'           => true,
        'rewrite'             => array('slug' => _x('project', 'URL slug', 'hsp')),
        'capability_category' => 'post',
        'has_archive'         => true,
        'hierarchical'        => false,
        'menu_position'       => 56,
        'menu_icon'           => 'dashicons-category',
        'supports'            => array('title', 'thumbnail')
    ];
    register_post_type('project', $args);

    register_taxonomy('project_type', 'project', array(
        'labels'                => array(
            'name'              => _x('Types', 'taxonomy general name', 'hsp'),
            'singular_name'     => _x('Type', 'taxonomy singular name', 'hsp'),
            'search_items'      => __('Search types', 'hsp'),
            'all_items'         => __('All types', 'hsp'),
            'parent_item'       => __('Parent type', 'hsp'),
            'parent_item_colon' => __('Parent type:', 'hsp'),
            'edit_item'         => __('Edit type', 'hsp'),
            'update_item'       => __('Update type', 'hsp'),
            'add_new_item'      => __('Add new type', 'hsp'),
            'new_item_name'     => __('New type', 'hsp'),
            'menu_name'         => __('Types', 'hsp'),
        ),
        'hierarchical'      => true,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array('slug' => 'project-type')
    ));

    register_taxonomy('project_cat', 'project', array(
        'labels'                => array(
            'name'              => _x('Categories', 'taxonomy general name', 'hsp'),
            'singular_name'     => _x('Category', 'taxonomy singular name', 'hsp'),
            'search_items'      => __('Search categories', 'hsp'),
            'all_items'         => __('All categories', 'hsp'),
            'parent_item'       => __('Parent category', 'hsp'),
            'parent_item_colon' => __('Parent category:', 'hsp'),
            'edit_item'         => __('Edit category', 'hsp'),
            'update_item'       => __('Update category', 'hsp'),
            'add_new_item'      => __('Add new category', 'hsp'),
            'new_item_name'     => __('New category', 'hsp'),
            'menu_name'         => __('Categories', 'hsp'),
        ),
        'hierarchical'      => true,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array('slug' => 'project-cat')
    ));
}

add_filter('register_post_type_args', function($args, $post_type){

    if($post_type === 'wp_block')
    {
        $args['public'] = true;
    }

    return $args;
}, 20, 2);
