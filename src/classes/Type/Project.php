<?php 

namespace HSP\Type;

// use HSP\Utils_Project;
use HSP\Utils;
use HSP\Utils_Format;

class Project extends Post
{
    public function __construct($post=null)
    {
        $this->post_type = 'project';

        parent::__construct($post, self::_get_props_config());
    }

    static function _get_props_config()
    {
        return [
            'area' => [
                'type' => 'meta'
            ],
            'cost' => [
                'type' => 'meta'
            ],
            'year' => [
                'type' => 'meta'
            ],
            'project_type' => [
                'type' => 'taxonomy'
            ],
            'project_cat' => [
                'type' => 'taxonomy'
            ]
        ];
    }

    public function getArea($format=false)
    {
        $meta = (int)$this->get_meta('area', true, 0);
        return $format ? Utils_Format::formatArea($meta) : $meta;
    }

    public function getCost($format=false)
    {
        $meta = (int)$this->get_meta('cost', true, 0);
        return $format ? Utils_Format::formatPrice($meta) : $meta;
    }

    public function getYear()
    {
        return $this->get_meta('year', true, 0);
    }

    public function getCategory($names=false)
    {
        $fields = $names ? 'names' : 'ids';
        return $this->getTerms('project_cat', $fields, true);
    }

    /*
    Images
    -------------------------
    */

    public function getGalleryImageIds()
    {
        return Utils::parseArrInts($this->get_meta('project_gallery', true, []));
    }
}
